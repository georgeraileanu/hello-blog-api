using hello_blog_api.Models;
using System.Collections.Generic;
using System.Linq;


namespace hello_blog_api.TestData
{
    public static class BlogPostTestData
    {
         ///<summary>Increments the Id of each blog post when a new record is created.</summary>
        private static int _id = 0;

        private static List<BlogPostModel> _blogPosts = new List<BlogPostModel>();

        //private readonly ILogger<BlogPostModel> _logger = new ILogger<BlogPostModel>;


        public static BlogPostModel GetBlogPostById(int id)
        {
            BlogPostModel blogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Id == id);
            return blogPostModel;
        }

        public static BlogPostModel GetBlogPostByTag(string tag)
        {
            BlogPostModel blogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Tags.Contains(tag) );
            return blogPostModel;
        }

        public static void DeleteBlogPostById(BlogPostModel blogPost)
        {
            _blogPosts.Remove(blogPost);
        }

        public static void UpdateBlogPost(int idOldBlogPost, BlogPostModel newBlogPost)
        {
            BlogPostModel oldBlogPostModel =_blogPosts.FirstOrDefault(blogPost => blogPost.Id == idOldBlogPost);
            
            var index = _blogPosts.IndexOf(oldBlogPostModel);

            if( index != -1)
            {
                _blogPosts[index] = newBlogPost;
            }
        }

        public static List<BlogPostModel> GetAllBlogPosts()
        {
            return _blogPosts;
        }

        public static bool AddBlogPost(BlogPostModel blogPost)
        {
            blogPost.Id = ++_id;
            _blogPosts.Add(blogPost);
            return true;
        }

        public static bool AddPutBlogPost(BlogPostModel blogPost)
        { 
            _blogPosts.Add(blogPost);
            return true;
        }

        public static bool ValidateBlogPostModel(BlogPostModel blogPost)
        {
            bool isValid = true;
            if (string.IsNullOrWhiteSpace(blogPost.Title))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Content))
            {
                isValid=false;
            }
            if (string.IsNullOrWhiteSpace(blogPost.Label))
            {
                isValid=false;
            }
            return isValid;
        }
    }
}