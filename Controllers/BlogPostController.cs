using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using hello_blog_api.TestData;
using System.Net;
using System.Collections.Generic;
using System.Collections;
using System;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController: Controller
    {
        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult CreateBlogPost([FromBody] BlogPostModel blogPost)
        {
            try
            {
                if (!BlogPostTestData.ValidateBlogPostModel(blogPost))
                {
                    return BadRequest("Model is not valid!");
                }
               
                if(!BlogPostTestData.AddBlogPost(blogPost))
                {   
                     return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                
                return CreatedAtAction(nameof(CreateBlogPost), blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a list of all blogs.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public IActionResult GetAllBlogPosts()
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.GetAllBlogPosts();
                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Gets a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public IActionResult GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

           ///<summary>
        ///Gets a blog based on the tag.
        ///Endpoint url: api/v1/BlogPost/tag/{blogTag}
        ///</summary>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("tag/{blogPostTag}")]
        public IActionResult GetBlogPostByTag([FromRoute] string blogPostTag)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostByTag(blogPostTag);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        ///<summary>
        ///Deletes a blog based on the blogId.
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{blogPostId}")]
        public IActionResult DeleteBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                else
                {
                    BlogPostTestData.DeleteBlogPostById(blogPost);
                    return Ok("Object deleted successfully!");
                }
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

         ///<summary>
        ///Updates a blog based on the blogId or creates it if it wasn't found
        ///Endpoint url: api/v1/BlogPost/{blogId}
        ///</summary>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Route("{blogPostId}")]
        public IActionResult UpdateBlogPostById([FromRoute] int blogPostId, [FromBody] BlogPostModel newBlogPost)
        {
            try
            {
                BlogPostModel oldBlogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (oldBlogPost == null)
                {
                    if (!BlogPostTestData.ValidateBlogPostModel(newBlogPost))
                    {
                        return BadRequest("Model is not valid!");
                    }
               
                    if(!BlogPostTestData.AddPutBlogPost(newBlogPost))
                    {   
                         return StatusCode((int)HttpStatusCode.InternalServerError);
                    }
                
                    return CreatedAtAction(nameof(CreateBlogPost), newBlogPost);
                    
                }
                else
                {
                    BlogPostTestData.UpdateBlogPost(blogPostId, newBlogPost);
                    return Ok("Object updated successfully!");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }



    }
}